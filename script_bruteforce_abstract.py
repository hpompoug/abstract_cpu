from acpu.cpu import Backend, ROB, CPU, Renamer
from acpu.muop import MuopToyFactory
from acpu.analysis import SensitivityEngine

# Architectural parameters
rob_size = 3
backend_tp = 1.0
ports = ["p0","p1","p2"]
# Code parameters
max_ports_per_op = 2
num_different_ops = 5
stream_size = 9
niters=1000
# Search parameters
search=True
search_many=False
search_unrolled= False
timeout=100

verbosity_level = 2
niters_print=1

# Checks
assert(len(ports) > 0)
assert(max_ports_per_op > 0)
assert(len(ports) > max_ports_per_op)

class CPUImpl(CPU):
    def is_shiny_muop(self,nop):
        return (
            self.backend.stalling[nop.port] and
            self.backend.is_majority(nop.port)
        )

class SEImpl(SensitivityEngine):

    def is_shiny_nominal_run(self):
        return (
            self.shiny_muops
        )
    
def __main__():

    cpu = CPUImpl(ports=ports,backend_tp=backend_tp,rob_size=rob_size)

    count = 0
    while True:
        prompt = False

        factory = MuopToyFactory(
            ports=ports,
            max_ports_per_op=max_ports_per_op,
            num_different_ops=num_different_ops
        )
        
        stream = [factory.build() for x in range(stream_size)]

        ae = SEImpl(
            cpu=cpu,
            stream=stream,
            niters=niters,
            window=niters_print,
            verbose=verbosity_level
        )

        ae.full_analysis(
            search=search,
            search_many=search_many,
            search_unrolled=search_unrolled,
        )

        if ae.shiny_sensitive:
            print(f"({count} attempts) ===\n")
            print(ae.report())
            count = 0
            prompt = True

        count += 1
        if count > timeout:
            print(f"Timeout ({count} attempts)")
            count = 0
            prompt = True
        
        if prompt:
            ncont = None
            while (ncont != "y" and ncont != "n" and ncont != ""):
                ncont = input("Continue ? [Y/n/p]")
                if ncont == "p":
                    print(ae.pythonify_stream())
                    
            if ncont == "n":
                break

__main__()
