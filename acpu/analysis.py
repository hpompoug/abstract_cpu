class AnalysisEngine:

    def __init__(self,cpu,stream,niters,window,verbose=0):
        self.cpu = cpu
        self.ports = cpu.backend.ports()
        self.stream = stream
        self.niters = niters
        self.window = window
        assert(self.window <= self.niters)

        self.nominal_done = False
        self.nominal_port_mapping = None
        self.nominal_sats = {}
        self.nominal_str_of_history = None
        self.nominal_str_of_saturation = None
        self.nominal_cycles = None
        self.shiny_muops = []

        self.verbose = verbose

    def str_of_stream(self):
        res = []
        for mu in self.stream:
            str_of_mu = mu.to_string(
                all_ports=False,
                mapped_ports=False,
                timestamp=False,
                decorate=False
            )
            res.append(str_of_mu)
        return " ".join(res)
        
    def simulate(self,search,search_many,search_unrolled):
        self.cpu.simulate(
            stream=self.stream,
            iterations=self.niters,
            search=search,
            search_many=search_many,
            search_unrolled=search_unrolled,
        )

    def simulate_nominal(
            self,
            search=False,
            search_many=False,
            search_unrolled=False,
    ):
        self.simulate(search,search_many,search_unrolled)
        self.shiny_muops = self.cpu.found
        for p in self.ports:
            self.nominal_sats[p] = self.cpu.backend.saturation(p)
        self.nominal_str_of_history = self.cpu.rob.str_of_history(
            length=len(self.stream)*self.window
        )
        self.nominal_str_of_saturation = str(self.cpu.backend)
        self.nominal_port_mapping = self.cpu.renamer.port_mapping
        self.nominal_cycles = self.cpu.backend.last_ts
        self.nominal_done = True

    def is_shiny_nominal_run(self):
        return True
        
    def report(self):
        str_of_program = self.str_of_stream()
        report = f"Program: {str_of_program}\n"
        if self.nominal_done:
            report += "\n"
            report += f"For {self.niters} iterations: "
            report += f"{self.nominal_cycles} cycles\n"
            report += "\n"
            report += f"Portmapping & timing\n{self.nominal_str_of_history}"
            report += "\n"
            report += f"Saturation\n{self.nominal_str_of_saturation}\n"
        return report

    def pythonify_stream(self,streamvar="stream"):
        str_of_stream = f"{streamvar} = ["
        for m in self.stream:
            sports = "[" + ",".join([f"\'{p}\'" for p in m.ports]) + "]"
            str_of_stream += f"\n  Muop(name=\'{m.name}\',ports={sports}),"
        str_of_stream += "\n]"
        return str_of_stream
    
class SensitivityEngine(AnalysisEngine):
    
    def __init__(self,cpu,stream,niters,window,verbose):

        super().__init__(
            cpu=cpu,
            stream=stream,
            niters=niters,
            window=window,
            verbose=verbose
        )

        self.shiny_sensitive = False
        self.str_of_sens = None
        self.influence = {}
        self.influence_percent = {}
        for p in self.ports:
            self.influence[p] = None
            self.influence_percent[p] = None
        self.acc_str_of_history = {}
        self.sens_str_of_history = {}
        self.rel_str_of_history = {}

    def simulate_sensitive(self,port):
        
        if self.influence[port] != None:
            return
        
        self.cpu.backend.accelerate(port)
        self.cpu.simulate(stream=self.stream,iterations=self.niters)
        self.cpu.backend.slowdown(port)

        for (i,tt) in self.shiny_muops:
            self.cpu.rob.history[i].flag = True
        
        self.influence[port] = self.nominal_cycles - self.cpu.backend.last_ts
        self.influence_percent[port] = (self.influence[port]/self.nominal_cycles)*100
        
    def full_analysis(
            self,
            search=False,
            search_many=False,
            search_unrolled=False
    ):
        self.simulate_nominal(search,search_many,search_unrolled)
        if not self.is_shiny_nominal_run():
            return
        for p in self.ports:
            self.simulate_sensitive(p)
            self.acc_str_of_history[p] = self.cpu.rob.str_of_history(
                length=len(self.stream)*self.window
            )
            if self.influence[p] > 0.0:
                self.sens_str_of_history[p] = self.acc_str_of_history[p]

            if self.is_poison_sensitive_run(p):
                self.shiny_sensitive = False
                break
            if self.is_shiny_sensitive_run(p):
                self.shiny_sensitive = True
                self.rel_str_of_history[p] = self.acc_str_of_history[p]
        
    def is_shiny_sensitive_run(self,p):
        return True

    def is_poison_sensitive_run(self,p):
        return False
        
    def report(self):
        report = super().report()
        
        report += "Sensitivity\n"
        for p in self.ports:
            speedup_str = str(self.influence[p])
            speedup_percent_str = '%.2f' % self.influence_percent[p]
            sens= f"{p}: {speedup_percent_str}% ({speedup_str} cycles)"
            report += f"{sens}\n"

        acc_dict = {}
        if self.verbose == 3:
            acc_dict = self.acc_str_of_history
        if self.verbose == 2:
            acc_dict = self.sens_str_of_history
        elif self.verbose == 1:
            acc_dict = self.rel_str_of_history

        report += "\n"
        for (p,r) in acc_dict.items():
            report += f"{p} accelerated:\n{r}\n"
            
        return report
