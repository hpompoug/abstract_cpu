from random import choice
import math

# TODO: changer l'algorithme d'allocation pour raisonner à l'échelle de tout
# le ROB (ce qui va peut-être nécessiter de changer le ROB aussi)

class Renamer:

    def __init__(self):
        self.clear()
    
    def clear(self):
        self.port_mapping = []
    
    def randomly_map(self,nop):
        nop.port = choice(nop.ports)

    def map(self,nop,backend):
        slots = {}
        for p in nop.ports:
            slots[p] = backend.first_slot_free(p) + backend.throughputs[p]
        free_port = min(slots, key=slots.get)
        nop.port = free_port
        
class Backend:

    def __init__(self, ports, default_throughput=1.0):
        self.throughputs = {}
        for c in ports:
            self.throughputs[c] = default_throughput

    def clear(self):
        self.last_ts = 0.0
        self.prev_ts = {}
        self.card_ports = {}
        self.stalling = {}
        for c in self.throughputs:
            self.card_ports[c] = 0
            self.stalling[c] = False

    def ports(self):
        return [p for p in self.throughputs]
        
    def accelerate(self,r):
        assert(r in self.throughputs)
        self.throughputs[r] = self.throughputs[r]/2

    def slowdown(self,r):
        assert(r in self.throughputs)
        self.throughputs[r] = self.throughputs[r]*2
        
    def issue(self,nop):
        if nop.timestamp > 0.0:
            prec = self.prev_ts[nop.port] if nop.port in self.prev_ts else 0.0
            stall = nop.timestamp - prec - self.throughputs[nop.port]
            self.stalling[nop.port] = stall > 0.0
        self.prev_ts[nop.port] = nop.timestamp
        self.last_ts = max(
            self.last_ts,
            nop.timestamp + self.throughputs[nop.port]
        )
        self.card_ports[nop.port] += 1

    def is_majority(self,port):
        bound = self.card_ports[port]
        for (r,n) in self.card_ports.items():
            if n > bound:
                return False
        return True
            
    def first_slot_free(self,port):
        if port in self.prev_ts:
            slot = (self.prev_ts[port]
                    + self.throughputs[port])
        else:
            slot = 0
        return slot

    def saturation(self,port):
        if self.card_ports[port]:
            card = self.card_ports[port]
            sat = (card/self.last_ts)*100 if self.last_ts else 0.0
        else:
            sat = 0.0
        return sat
    
    def report(self,vertical=True):
        sep = "\n" if vertical else "  "
        rep = ""
        for p in self.card_ports:
            sat = '%.2f' % self.saturation(p)
            rep += f"{p}:{sat}%" + sep
        return rep

    def __str__(self):
        return self.report()
    
class ROB:

    def __init__(self,size=4):
        self.size = size
        self.clear()

    def clear(self):
        self.history = []
        self.buff = []

    def full(self):
        return len(self.buff) == self.size

    def empty(self):
        return len(self.buff) == 0

    def tail(self):
        assert(len(self.buff))
        return self.buff[-1]

    def head(self):
        assert(len(self.buff))
        return self.buff[0]

    def rotate(self):
        assert(not self.empty())
        self.buff = self.buff[1:]

    def insert(self,nop):
        assert(len(self.buff) < self.size)
        self.buff.append(nop)
        self.history.append(nop)

    def str_of_history(self,vertical=True,length=-1):
        sep = "\n" if vertical else " // "
        res = ""
        for i,m in enumerate(self.history):
            res += str(m) + sep
            if i+1 == length:
                if i+1 < len(self.history):
                    res += "...\n"
                break
        return res
    
class CPU:

    def __init__(self,ports,backend_tp,rob_size):
        self.renamer = Renamer()
        self.backend = Backend(ports=ports,default_throughput=backend_tp)
        self.rob = ROB(size=rob_size)
        self.clear()
    
    def clear(self):
        self.found = []
        self.rob.clear()
        self.backend.clear()
        self.renamer.clear()

    def simulate(
            self,
            stream,
            iterations=1,
            search=False,
            search_many=False,
            search_unrolled=False,
    ):

        self.clear()
        
        offset = 0
        
        counter = 0
        for i in range(0,iterations):
            for nop in stream:
                nop = nop.clone()
                # Retire the oldest muop
                if self.rob.full():
                    oop = self.rob.head()
                    noffset = (oop.timestamp
                               + self.backend.throughputs[oop.port])
                    offset = math.ceil(max(noffset,offset))
                    self.rob.rotate()
                # Insert a new muop
                self.renamer.map(nop,self.backend)
                self.rob.insert(nop)
                # Issue the new muop
                timecand = max(self.backend.first_slot_free(nop.port),offset)
                ntp = self.backend.throughputs[nop.port]
                if timecand % ntp > 0.0:
                    timecand = math.ceil(timecand/ntp)
                nop.timestamp = timecand
                self.backend.issue(nop)
                # Out if stop conditions are met
                if (
                        search and
                        (search_many or len(self.found) == 0) and
                        (search_unrolled or counter < len(stream)) and
                        self.is_shiny_muop(nop)
                ):
                    self.found.append((counter,nop.timestamp))
                    nop.flag = True
                    
                counter += 1

        self.backend.last_ts = math.ceil(self.backend.last_ts)

    def is_shiny_muop(self,nop):
        return False
