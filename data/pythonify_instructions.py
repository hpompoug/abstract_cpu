import xml.etree.ElementTree as ET
import re

filename = 'instructions.xml'
indent_of = 2
isa_name = "isa"
ports_name = "ports"

def __main__():

    root = ET.parse(filename)
    muops = []
    ports = []

    indent = " " * indent_of
    
    for instrNode in root.iter('instruction'):

        if instrNode.attrib['extension'] != 'BASE':
            continue
            
        name = instrNode.attrib['asm']
        archiNodes = [x for x in instrNode.iter('architecture')
                      if x.attrib['name'] in ['SKL']]
        measuNodes = []
        if archiNodes:
            archiNode = archiNodes[0]
            measuNodes = [x for x in archiNode.iter('measurement')
                          if x.attrib['uops'] == "1"]
        if (
                measuNodes
                and "load" not in name
                and "store" not in name
                and "disp32" not in name
                and "LEA" not in name
                # TODO match family instead of name
                and "J" not in name
                and "SET" not in name
                and "BT" not in name
                and "LAHF" not in name
                and "CMOV" not in name
        ):
            measuNode = measuNodes[0]
            match = re.search(r'1\*p([0-9]+)', measuNode.attrib['ports'])

            lports=[]
            for p in list(match.group(1)):
                sport = f"\'p{p}\'"
                if sport not in ports:
                    ports.append(sport)
                lports.append(sport)
            slports = "[" + ",".join(lports) + "]"

            smuop = f"Muop(name=\'{name}\',ports={slports})"
            if smuop not in muops:
                muops.append(smuop)

    imports = "from acpu.muop import Muop"
    smuops = isa_name + " = [\n" + indent + (",\n" + indent).join(muops) + "\n]"
    sports = ports_name + " = [" + ",".join(ports) + "]"

    print(imports)
    print(smuops)
    print(sports)

__main__()
    
