from acpu.cpu import Backend, ROB, CPU, Renamer
from acpu.muop import Muop,MuopFactory
from acpu.analysis import SensitivityEngine
from acpu.skylake_minimalist import isa,ports

# Architectural parameters
rob_size = 4
backend_tp = 1.0
# Code parameters
stream_size = 13
niters=1
max_mean_possible_ports = 2
max_possible_ports = stream_size * max_mean_possible_ports
# Search parameters
max_cycles=4
search=True
search_many=False
search_unrolled= False
timeout=1000
# Sensitivity parameters
min_cycles_speedup = 1.0
min_percent_speedup = 5.0
is_saturated_at = 100.0
# Print parameters
niters_print_window=1
verbosity_level = 2

skeleton = None

skeleton = [
    Muop(name='MUL',ports=['p1']),
    Muop(name='SBB',ports=['p0','p6']),
    Muop(name='ROL',ports=['p0','p6']),
    Muop(name='BSF',ports=['p1']),
    Muop(name='ROL',ports=['p0','p6']),
    Muop(name='POP',ports=['p2','p3']),
    Muop(name='MOV',ports=['p2','p3']),
    Muop(name='SAHF',ports=['p0','p6']),
    Muop(name='MOVSX',ports=['p2','p3']),
    Muop(name='SAHF',ports=['p0','p6']),
    Muop(name='SBB',ports=['p0','p6']),
    Muop(name='XOR',ports=['p0','p1','p5','p6']),
]

class CPUImpl(CPU):
    
    def is_shiny_muop(self,nop):
        return (
            self.backend.stalling[nop.port] and
            self.backend.is_majority(nop.port)
        )

class SEImpl(SensitivityEngine):

    def is_shiny_nominal_run(self):
        return (
            self.shiny_muops and
            self.nominal_cycles <= max_cycles and
            max(self.nominal_sats.values()) >= is_saturated_at
        )
    
    def is_shiny_sensitive_run(self,p):
        
        has_varied = False
        for (i,tt) in self.shiny_muops:
            has_varied = has_varied or self.cpu.rob.history[i].timestamp != tt
            
        return (
            has_varied and
            self.influence[p] >= min_cycles_speedup and
            self.influence_percent[p] >= min_percent_speedup and
            self.nominal_sats[p] <= 50.0
        )

    def is_poison_sensitive_run(self,p):
        same_port_mapping = all(x == y for x, y
                                in zip(self.nominal_port_mapping,
                                       self.cpu.renamer.port_mapping))
        
        return (
            not same_port_mapping or
            (self.influence[p] >= min_cycles_speedup and
            self.influence_percent[p] >= min_percent_speedup and
            self.nominal_sats[p] >= is_saturated_at)
        )

def __main__():

    factory = MuopFactory(muops=isa,ports=ports)
    cpu = CPUImpl(ports=ports,backend_tp=backend_tp,rob_size=rob_size)

    count = 0
    while True:

        stream = []
        if skeleton:
            stream = factory.fill(stream=skeleton)
        else:
            stream = [factory.build() for x in range(stream_size)]

        nports = 0
        for m in stream:
            nports += len(m.ports)
        if nports > max_possible_ports:
            continue
            
        ae = SEImpl(
            cpu=cpu,
            stream=stream,
            niters=niters,
            window=niters_print_window,
            verbose=verbosity_level
        )
        
        ae.full_analysis(
            search=search,
            search_many=search_many,
            search_unrolled=search_unrolled,
        )

        count += 1

        if ae.shiny_sensitive or count > timeout:
            print(f"({count} attempts)")
            if ae.shiny_sensitive:
                print(ae.report() + "="*3)
            count = 0
        
            ncont = None
            while (ncont != "y" and ncont != "n" and ncont != ""):
                ncont = input("Continue ? [Y/n/p]")
                if ncont == "p":
                    print(ae.pythonify_stream())
                    
            if ncont == "n":
                break

__main__()
