from acpu.cpu import Backend, ROB, CPU, Renamer
from acpu.muop import Muop
from acpu.analysis import SensitivityEngine
from acpu.skylake import isa,ports

# Architectural parameters
rob_size = 4
backend_tp = 1.0
# Execution parameters
niters=100

niters_print=1
verbosity_level=3

stream = [
  Muop(name='MUL',ports=['p1']),
  Muop(name='SBB',ports=['p0','p6']),
  Muop(name='ROL',ports=['p0','p6']),
  Muop(name='BSF',ports=['p1']),
  Muop(name='ROL',ports=['p0','p6']),
  Muop(name='POP',ports=['p2','p3']),
    Muop(name='MOV',ports=['p2','p3']),
  Muop(name='SAHF',ports=['p0','p6']),
  Muop(name='MOVSX',ports=['p2','p3']),
  Muop(name='SAHF',ports=['p0','p6']),
  Muop(name='SBB',ports=['p0','p6']),
  Muop(name='XOR',ports=['p0','p1','p5','p6']),
]

def __main__():

    cpu = CPU(ports=ports,backend_tp=backend_tp,rob_size=rob_size)

    ae = SensitivityEngine(
        cpu=cpu,
        stream=stream,
        niters=niters,
        window=niters_print,
        verbose=verbosity_level
    )
    
    ae.full_analysis()
    
    print(ae.report())

__main__()
